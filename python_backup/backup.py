#!/usr/bin/env python
##
## fichier: backup.py
## author: Thomas GRAULLE
## contact: thomasgraulle@gmail.com OR TRdgXyjFby9WPyZvTrNH1gcjWxRsBrN@cock.li
## date: 2/12/2019
## description: ce programme permet de faire un backup
## readme: non
## version 0.2
## update: 08/12/2019 Thomas GRAULLE, Ajout de fonctionnalité:
##      - Utilisation d'une class pour faire le backup
##      - Mise en place de la possiblité de faire une copie de l'archive
## update: DD/MM/YYYY Prénom NOM, description
## evolution:
#       - Faire en sorte que le backup s'éxécute sans faire de commande shell, parce que pour l'instant ce que je fais c'est un peu de la merde
#       -
########################################################################################################################
## import
########################################################################################################################
# region Import
import subprocess
from os import path
from datetime import datetime
from random import randint
from shutil import copy2 as copy_file_lib
# endregion

########################################################################################################################
## Class
########################################################################################################################
# region Class
class Compress_file:
    """
    Compress_file: class permettant de faire une compression de fichier en .tar.gz

    ---------------------------------------------------
    attribut:
        output_name: string, Nom du fichier compressé en sortie du programme (exemple: "/home/`users`/fileOrFolder", "fileOrFolder")
        input_name: string, Nom du fichier ou dossier en entré. (exemple: "/home/`users`/fileOrFolder", "fileOrFolder")
        path_destination_copy: string, Chemin du dossier pour faire une copie, si nécessité.
        output_name_reformed: string, Chemin du dossier pour faire une copie, si nécessité.
        output_command: string, Sortie de la commande
        errors_command: string, Erreurs retourné par la commmande
    """

    def __init__(self, output_name, input_name, path_destination_copy = None):
        """
        __init__: Initialisation de la class

        :param output_name: string, Nom de la sortie du dossier
        :param input_name: string, Nom du fichier ou du dossier à compresser
        :param path_destination_copy: None|string, Chemin du dossier ou faire la copy de l'archive

        exemple: Zip_file("/home/`users`/dev/backup/tempBackup", "/home/`users`/dev/temp/", path_destination_copy = "/home/`users`/backup/")
        exemple: Zip_file("/home/`users`/dev/backup/temp_fichier_txt", "/home/`users`/dev/temp/fichier.txt", path_destination_copy = "/home/`users`/backup/")
        """
        self.output_name = output_name
        self.input_name = input_name
        self.path_destination_copy = path_destination_copy

        self.output_name_reformed = ""
        self.output_command = ""
        self.errors_command = ""

    def __compress_file(self):
        """
        __compress_file: Compresse un fichier ou un dossier

        :return: bool, renvoie True ou False si le backup s'éffectue bien
        """
        # Variable par défaut
        self.output_name_reformed = self.__complet_name(self.output_name)
        result = False

        # Execute the command if the target exist
        if path.exists(self.input_name):
            command = ['tar', '-zcvf', self.output_name_reformed, self.input_name]
            p = subprocess.Popen(command, stdout=subprocess.PIPE)
            output_command, errors = p.communicate(timeout=400)
            self.output_command = output_command.decode("utf-8") # converti le retour en binaire en string
            self.errors_command = "" if errors == None else errors.decode("utf-8") # converti le retour en binaire en string
            result = True

        else:
            self.output_command, self.errors = ('', "The target file does't exist.")
            result = False # Facultatif

        return result

    def __complet_name(self, file_name):
        """
        __complet_name: Retourne un nom de fichier bien formalisé et qui n'existe pas déjà

        :param file_name: string, Nom de sortie du fichier
        :return: string, Retourne la chaine un nom de fichier bien formalisé qui n'existe pas déjà
        """
        # Initialise the var
        today = datetime.now()
        complet_name = '{}{}'.format(file_name, today.strftime("_%Y-%m-%d-%H-%M-%S"))
        complet_output_name = "{}.tar.gz".format(complet_name)

        # Check if the file exist
        while path.exists(complet_output_name):
            complet_name_temp = "{}_{}".format(complet_name, str(randint(100000000, 999999999)))
            complet_output_name = '{}.tar.gz'.format(complet_name_temp)

        return complet_output_name


    def __copy(self):
        """
        __copy: Fait la copy d'un fichier dans le répertoire de copy

        :return: bool, Retourne True ou False en fonction de si la copie c'est bien faite ou non
        """
        result = False
        if self.path_destination_copy != None:
            if path.exists(self.path_destination_copy):
                try:
                    copy_file_lib(self.output_name_reformed, self.path_destination_copy)
                    result = True

                except Exception as e:
                    print(e)

        return result

    def execut(self):
        """
        execut: Exécute le script:
            - Compresse le fichier ou le dossier
            - Vérification de la validité de la compression (archive)
            - Copie de l'archive si nécessaire
            - Vérification de la copie

        :result: void
        """
        if self.__compress_file(): # Exécution de la compression et vérification de sa validité
            print("Compression du fichier effectué.")
            if self.path_destination_copy != None:
                if self.__copy(): # Exécution de la copy du fichier et de la vérification de sa validité
                    print("Copie de l'archive effectué.")
                else:
                    print("Il y a eu un problème lors de la copie de l'archive.")
        else:
            print("Il y a eu un problème lors de la compression du fichier ou du dossier.")
# endregion

########################################################################################################################
# --- python_backup
########################################################################################################################
# region python_backup
if __name__ == '__main__':
    compression = Compress_file("/home/stifler/Dev/temp/toto", "/home/stifler/Dev/temp/toto/", path_destination_copy="/home/stifler/Dev/temp/test1/")
    compression.execut()

    # command = ['tar', '-zcvf', 'archivePython.tar.gz', 'toto']
    # zip_file("/home/stifler/Dev/temp/toto", "/home/stifler/Dev/temp/toto")
# end region