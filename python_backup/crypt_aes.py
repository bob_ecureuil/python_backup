#!/usr/bin/env python
##
## fichier: backup.py
## author: Thomas GRAULLE
## contact: thomasgraulle@gmail.com OR TRdgXyjFby9WPyZvTrNH1gcjWxRsBrN@cock.li
## date: 2/12/2019
## description: ce programme permet de faire un backup
## readme: non
## version 0.2
## update: 08/12/2019 Thomas GRAULLE, Ajout de fonctionnalité:
##      - Utilisation d'une class pour faire le backup
##      - Mise en place de la possiblité de faire une copie de l'archive
## update: DD/MM/YYYY Prénom NOM, description
## evolution:
#       - Faire en sorte que le backup s'éxécute sans faire de commande shell, parce que pour l'instant ce que je fais c'est un peu de la merde
#       -
########################################################################################################################
## import
########################################################################################################################
# region Import
import pyAesCrypt
import argparse
# endregion

########################################################################################################################
## Class
########################################################################################################################
# region Class
class Crypt_AES:
    """
    Crypt_AES: class permettant de chiffrer et déchiffrer des fichiers en AES

    ---------------------------------------------------
    attribut:
        password: password pour chiffrer et déchiffrer
        buffer_size: buffer size
    """

    def __init__(self, password, buffer_size=(64 * 1024)):
        self.password = password
        self.buffser_size = buffer_size

    def encrypt_file(self, input, output):
        """
        encrypt_file: Chiffrement d'un fichier

        :param input: string, fichier que l'on veut chiffrer
        :param output: string, nom du fichier en sortie du programme

        :return: void
        """
        pyAesCrypt.encryptFile(input, output, self.password, self.buffser_size)

    def decrypt_file(self, input, output):
        """
        decrypt_file: Déchiffrement d'un fichier

        :param input: string, fichier que l'on veut déchiffrer
        :param output: string, nom du fichier en sortie du programme

        :retrun void
        """
        pyAesCrypt.decryptFile(input, output, self.password, self.buffser_size)

# endregion

########################################################################################################################
# --- python_backup
########################################################################################################################
# region python_backup
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=str, help="nom du fichier en entrée", default=None)
    parser.add_argument('-o', '--output', type=str, help="nom du fichier en sortie", default=None)
    parser.add_argument('-p', '--password', type=str, help="password", default=None)
    parser.add_argument('-b', '--buffer_size', type=int, help="buffer size", default=1048576)
    parser.add_argument('-a', '--action', type=str, help="action que l'on veut faire: (encrypt, decrypt)", default="encrypt")
    crypt_aes = parser.parse_args()

    if crypt_aes.input != None and crypt_aes.output != None and crypt_aes.action in ("encrypt", "decrypt") and crypt_aes.password != None:
        aes = Crypt_AES(crypt_aes.password, crypt_aes.buffer_size)
        if crypt_aes.action == "encrypt":
            aes.encrypt_file(crypt_aes.input, crypt_aes.output)
        else:
            aes.decrypt_file(crypt_aes.input, crypt_aes.output)

# end region