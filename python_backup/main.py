# ! /usr/bin/env python3
# -*- coding: utf-8 -*-
## fichier: backup_python.py
## author: Thomas GRAULLE
## contact: thomasgraulle@gmail.com OR TRdgXyjFby9WPyZvTrNH1gcjWxRsBrN@cock.li
## date: 08/12/2019
## description: ce programme permet la mise en place de backup avec l'envoie de log par mail
## readme: non
## version: 0.3
## update: 17/12/2019 Thomas GRAULLE, Correction des erreurs:
##      - Changement du chemin du fichier de configuration
## update: 22/12/2019 Thomas GRAULLE, Modification de l'envoie de mail:
##      - Possibilité d'envoie de mail avec pièce jointe
##      - Récupération des liens abosolue de façon dynamique
## update: DD/MM/YYYY Prénom NOM, description
## evolution:
##      - Mettre la possibilité d'envoyer l'archive sur un serveur via du SSH
##      - Trouver une façon plus sécurisé pour stocker le mot de passe. Peut être le mot de passe dans un fichier, et le chiffrer et le déchiffrer avec une clef RSA. Ce serait déjà bien mieux
##      - Faire en sorte que le chemin du fichier de configuration soit relatif et non absolu comme je le fais pour le moment
########################################################################################################################
## Import
########################################################################################################################
# region import
from mail import Email
from crypt_aes import Crypt_AES
from backup import Compress_file
from datetime import datetime
import os
import argparse
import configparser
# endregion

########################################################################################################################
## Constante
########################################################################################################################
# region Class
PATH_FOLDER = os.path.dirname(os.path.realpath(__file__))
CONFIG_FILE = "./config.ini" # todo le bon lien
# CONFIG_FILE = "./myconfig.ini" # todo remove this line
# endregion

########################################################################################################################
## Class
########################################################################################################################
# region Class
class Make_backup:
    """
    Make_backup: class permettant de faire un backup et d'envoyer les log par mail:
        - Fait le backup du dossier ou fichier
        - Le copy si nécessaire
        - Envoie les logs par mail

    ---------------------------------------------------
    attribut:
        backup_informations: obj, Information relative au backup
            .output: string, Chemin du fichier en sortant du programme. L'extension est géré par le programme (exemple: "/home/`users`/fileOrFolder", "fileOrFolder")
            .input: string, Chemin du dossier ou fichier que l'on veut compresser (exemple: "/home/`users`/folder", "fileOrFolder", "/home/`users`/file.txt")
            .copy: string, Chemin de copy d'une archive
        email_informations: obj, Information relative a l'email
            .subject: string, Sujet que l'on veut afficher sur l'email
            .email_from_showing: string, Destinaire que l'on veut afficher sur l'email (ex: "monadresse@gmail.com", "BOT backup")
            .port: string, Port pour se connecter au serveur SMTP  (généralement c'est le port 465)
            .smpt_adresse: string, Adresse pour se connecter au serveur SMTP (ex: mail.cock.li)
            .user_email: string, Email pour se connecter au serveur SMPT, souvent le même que le "user_login"
            .user_login: string, Loging pour se connecter au serveur SMTP souvent le même que le "user_email"
            .user_password: string, Mot de passe pour se connecter, un chemin de fichier si le mot de passe est chiffré
            .password_encryption: bool, Booléan affirmant si le mot de passe est chiffré
            .log_file_encrypted: bool, Booléan affirmant si les fichiers de log son chiffré
            .receiver_email: list, Liste des adresses de destination
        logs: obj, Objet contenant les log
            .errors_command: string, Erreur retourner par la commande
            .output_command: string, Contenu retourner par la commande
        files: array of tuple(string, bool), tableau contenant les fichiers que l'on veut mettre en pièce jointe et si on veut l'envoyé par email.  (exemple: [("/home/users/log/mylog.txt"], True))
    """

    def __init__(self, backup_informations, email_informations, encryption_informations):
        """
        __init__:

        :param backup_informations: obj, objet avec toutes les informations pour le backup
        :param email_informations, obj, objet avec toutes les informations pour l'envoie d'email

        :return: void,
        """
        self.backup_informations = backup_informations
        self.email_informations = email_informations
        self.encryption_informations = encryption_informations
        self.logs = type('', (object,), {})()
        self.files = []

    def do_bakup(self):
        """
        do_backup: Fait le backup des fichiers, et la copie

        :return: void
        """
        backup = Compress_file(
            self.backup_informations.output,
            self.backup_informations.input,
            self.backup_informations.copy
        )
        # Lancement du script de backup
        backup.execut()

        # Récupération des logs
        self.logs.output_command = backup.output_command
        self.logs.errors_command = backup.errors_command
        # self.logs_reformed()

    def do_email(self):
        """
        do_email: Envoie le mail:
            - Création des fichiers de log
            - Création de l'objet pour la création de mail
            - Configuration de l'envoie de mail
            - Ajout du contenu
            - Envoie du mail
            - Suppression du fichier de log

        :return: void
        """
        # Pièces jointes
        self.create_log_file()

        # Informations sur l'email
        email = Email(
            self.email_informations.user_login,
            self.email_informations.user_password,
            self.email_informations.smpt_adresse,
            self.email_informations.port,
            self.email_informations.user_email
        )
        # Configuration des paramètres
        for a_email in self.email_informations.receiver_email:
            email.add_receiver_email(a_email) # Ajout des destinataires

        email.subject = self.email_informations.subject
        email.sender_email_showing = self.email_informations.email_from_showing

        # Remplissage du contenu de l'email
        email.add_content("Un Backup du fichier \"{}\" a été effectué".format(self.backup_informations.input))
        email.add_content("------------------------------------------------------------------------------------------")
        email.add_content("     Données renvoyées par la commande: ")
        email.add_content("     En pièce jointe ")
        email.add_content("------------------------------------------------------------------------------------------")
        email.add_content("     Erreurs renvoyes par la commande: ")
        email.add_content("Une erreur s'est produite pendant l'exécution de la commande"
                          if self.logs.errors_command != ""
                          else "Aucune erreur s'est produite pendant l'exécution ")
        email.add_content("------------------------------------------------------------------------------------------")

        # Ajout des pièces jointes dans le mail
        for file, attachment in self.files:
            if attachment:
                email.add_file(file)

        # Envoie de l'email
        email.send_email()

        # Suppression du fichier de log
        self.remove_log_file()

    def remove_log_file(self):
        """
        remove_log_file: Supprime simplement les fichiers de logs

        :return: Void
        """
        for file, _ in self.files:
            os.remove(file)

    def create_log_file(self):
        """
        create_log_file: Créer le fichier de log.

        :return: void
        """
        # Nom des fichiers
        file_name = "logFile{}.txt".format(datetime.now().strftime("_%Y-%m-%d-%H-%M-%S"))
        full_name = "{}/../temp/{}".format(PATH_FOLDER, file_name)

        # Création du fichier de log
        with open(full_name, "a+") as file_open:
            file_open.write("Un Backup du fichier \"{}\" a ete effectue \n".format(self.backup_informations.input))
            file_open.write("---------------------------------------------------------------------------------------\n")
            file_open.write("     Données renvoyées par la commande: \n")
            file_open.write(self.logs.output_command)
            file_open.write("---------------------------------------------------------------------------------------\n")
            file_open.write("     Erreurs renvoyes par la commande: \n")
            file_open.write(self.logs.errors_command)
            file_open.write("---------------------------------------------------------------------------------------\n")

        # Vérification de la nécessité de chiffrer le fichier de log
        if self.email_informations.log_file_encrypted:
            # Chiffre le fichier de log
            aes = Crypt_AES(self.encryption_informations.password_for_email_attachment, self.encryption_informations.buffer_size)
            full_name_encrypt = full_name + ".aes"
            aes.encrypt_file(full_name, full_name_encrypt)

            # Ajout dans les variables
            self.add_file((full_name, False))
            self.add_file((full_name_encrypt, True))
        else:
            self.add_file((full_name, True))

    def execute(self):
        """
        execute: Cette fonction fait:
            - Fait le backup
            - Envoie le mail avec les informations des logs du backup

        :return: void
        """
        self.do_bakup()
        self.do_email()

    def uncrypt_password(self):
        """
        uncrypt_password: Renvoi le mot de passe déchiffré.
            Laisse le mot de passe de base si le mot de passe n'est pas chiffré.

        # todo finir la partie pour déchiffrement de mot de passe

        :return: string, Retourne le bon mot de passe
        """

        password_base = self.email_informations.user_password # mot de passe de base

        if self.email_informations.password_encryption: # Je regarde si le mot de passe est chiffré
            print("déchiffrer le mot de passe") # todo finir cette partie
            password = ""
        else:
            password = password_base

        return password

    def add_file(self, file):
        """
        add_file: Ajoute un fichier à la liste de fichier à envoyer

        :param file: tuple(string, bool), chemin du fichier et si on l'attache à l'email

        :return: void
        """
        self.files.append(file)
# endregion

########################################################################################################################
## Fonction
########################################################################################################################
# region Function
def get_param(section, param):
    """
    get_param: Récupère la valeur d'une variable du fichier de configuration défini dans la constante au début de ce fichier.

    :param section
    :param param

    :return mixed | None: Retourne la valeur du paramètre du fichier de configuration ou retourne 'None' si erreur.
    """
    configuration = configparser.ConfigParser()
    configuration.read_file(open("{}/{}".format(PATH_FOLDER, CONFIG_FILE)))
    try:
        result = configuration.get(section, param)
        result = result.split(";")[0] # Je supprime tout ce qu'il y a après le caractère ';'
        if "," in result:
            result = string_to_list(result)
    except Exception as e:
        print(e)
        result = None

    return result

def string_to_list(string):
    """
    string_to_list: Converti une chaine de caractère en liste. Chaque paramètre sont séparé par le caractère ","
        Je sais qu'il doit exister des fonction qui fait ça, mais flemme de chercher

    :param: string, string, Chaine de caractère avec le caractère "," pour séparer le tout.

    :return: list, Retourne la liste de paramètre contenu dans la string.
    """
    # Variable par défaut
    list = []
    temp_string = ""

    # Traitement, ajoute dans la liste chaque contenu coupé par le caractère ","
    for char in string:
        if char == ",": # Il y a une virgule donc c'est un paramètre
            list.append(temp_string)
            temp_string = ""
        else:
            temp_string += char

    return list

# endregion

########################################################################################################################
## Main
########################################################################################################################
# region backup_python
if __name__ == "__main__":
    # Récupération des paramètres d'appels dans des objets anonyme
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=str, help="nom du fichier a compresser", default="/home/`users`/")
    parser.add_argument('-o', '--output', type=str, help="nom du fichier après compression (sans les extensions)", default="/tmp/backup")
    parser.add_argument('-c', '--copy', type=str, help="chemin du dossier pour faire une copie", default=None)
    backup_informations = parser.parse_args()

    emailInformations = type('', (object,), {})()
    emailInformations.subject = get_param("email", "subject")
    emailInformations.email_from_showing = get_param("email", "emailFromShowing")
    emailInformations.port = get_param("email", "port")
    emailInformations.smpt_adresse = get_param("email", "smptAdresse")
    emailInformations.user_email = get_param("email", "userEmail")
    emailInformations.user_login = get_param("email", "userLogin")
    emailInformations.user_password = get_param("email", "userPassword")
    emailInformations.password_encryption = get_param("email", "passwordEncryption")
    emailInformations.receiver_email = get_param("email", "receiverEmail")
    emailInformations.log_file_encrypted = get_param("email", "logFileEncrypted")

    encryptionInformations = type('', (object,), {})()
    encryptionInformations.buffer_size = int(get_param("crypt", "bufferSize"))
    encryptionInformations.password_for_email_attachment = get_param("crypt", "passwordForEmailAttachment")

    # Appelle de la class 'Make_backup'
    the_make_backup = Make_backup(backup_informations, emailInformations, encryptionInformations)
    the_make_backup.execute()

# endregion
