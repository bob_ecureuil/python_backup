#!/usr/bin/env python
##
## fichier: mail.py
## author: Thomas GRAULLE
## contact: thomasgraulle@gmail.com OR TRdgXyjFby9WPyZvTrNH1gcjWxRsBrN@cock.li
## date: 2/12/2019
## description: ce programme implémente une class qui permet d'envoyer un mail (smtp)
## readme: non
## version: 0.3
## update: 8/12/2019 Thomas GRAULLE, Prise en compte du sujet et de l'espéditeur
## update: 22/12/2019 Thomas GRAULLE, Changement de l'envoie de mail:
##      - Mail envoyé avec une pièce jointe avec dedans les logs
##      -
## update: DD/MM/YYYY Prénom NOM, description
## evolution:
##      - Mettre en place la possibilité de chiffrer le mail, pour pas qu'il soit en claire comme ça. Possibilité de chiffrer avec une solution openGPG
##      -
########################################################################################################################
## import
########################################################################################################################
# region import
import smtplib, ssl
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
# endregion

########################################################################################################################
## Class
########################################################################################################################
# region Class
class Email:
    """
    Email, class permettant d'envoyer des mails

    ---------------------------------------------------
    attribut:
        _password: string, mot de passe de l'utilisateur
        username: string, login de l'utilisateur (souvent l'adresse email de l'expéditeur)
        smtp_server: string, adresse du serveur smtp (ex: mail.cock.li)
        port: string, le port pour la connexion (généralement c'est le port 465)
        sender_email: string, l'adresse email de l'expéditeur
        sender_email_showing: string, l'adresse que l'on veut afficher comme expéditeur
        subject: string, sujet de l'email
        content: string, contenue du message envoyé
        files: array, liste des fichiers à envoyé en pièce jointe
        _receiver_email: array, liste des email destinataires

    """
    def __init__(self, username, password, smtp_server, port, sender_email):
        """
        __init__: initialisation de la class.

        :param username: login for the connexion
        :param password: password for the connexion
        :param smtp_server: adresse for the smtp connexion (ex: mail.cock.li)
        :param port: 465 by default
        :param sender_email: l'adresse email de l'expéditeur
        """
        self.username = username
        self._password = password
        self.smtp_server = smtp_server
        self.port = port
        self.sender_email = sender_email # my email
        self.sender_email_showing = "" # L'email que je veux afficher

        self.subject = ""
        self.content = ""
        self.files = [] # Fichier à envoier par pièce jointe
        self._receiver_email = [] # email target

    def add_content(self, a_content):
        """
        add_content: Ajoute du contenu pour l'envoie

        :param a_content:
        :return: void
        """
        self.content += a_content + "\n"

    def reset_content(self):
        """
        reset_content: rénitialise le contenu

        :return: void
        """
        self.content = ''

    def add_file(self, file):
        """
        add_file: Ajoute un fichier à la liste de fichier à envoyer

        :param file: string, fichier a ajouter au mail comme pièce jointe

        :return: void
        """
        self.files.append(file)

    def reset_file(self):
        """
        reset_file: Vide le tableau de fichier à ajouter en pièce jointe

        :return: void
        """
        self.files.clear()

    def add_receiver_email(self, email):
        """
        add_receiver_email: Ajoute dans la liste des destinataires un nouveau email si il n'est pas déjà inscrit

        :param email: string, adresse email a rajouter dans la liste
        """
        if not email in self._receiver_email:
            self._receiver_email.append(email)

    def send_email(self):
        """
        send_email, envoie l'email:
            - Récupère le contenu des fichiers attachés
            - Attache en pièce jointe le contenu des fichiers au mail
            - Envoie de l'email
            - Vide le contenu de la variable contenu et de la variable fichiers

        :return: bool, envoie True ou False en fonction de si ça c'est bien envoyé ou non
        """
        result = False
        if len(self._receiver_email) != 0:
            try:
                # Information relative à l'email
                msg = MIMEMultipart()
                msg['From'] = self.sender_email_showing
                msg['To'] = COMMASPACE.join(self._receiver_email)
                msg['Date'] = formatdate(localtime=True)
                msg['Subject'] = self.subject
                msg.attach(MIMEText(self.content))

                # Ajout des fichiers en pièce jointe
                for f in self.files or []:
                    with open(f, "rb") as fil:
                        part = MIMEApplication(
                            fil.read(),
                            Name=basename(f)
                        )
                    # After the file is closed
                    part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
                    msg.attach(part)

                # Envoie de l'email
                context = ssl.create_default_context()
                with smtplib.SMTP_SSL(self.smtp_server, self.port, context=context) as server:
                    server.login(self.sender_email, self._password)
                    server.sendmail(self.sender_email, self._receiver_email, msg.as_string())

                print("Message envoyé")
                result = True

            except Exception as e:
                result = False
                print(e)

        else:
            result = False
            print("Sender email is not define!")

        return result
# endregion

########################################################################################################################
## python_backup
########################################################################################################################
# region python_backup
if __name__ == '__main__':
    print("python_backup") # inutile
# endregion