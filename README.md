# python_backup

## Configuration du projet
Pour que le projet fonctionne normalement, il faut configurer avec vos informations le fichier __config.ini__.
Ensuite il faut allé dans le fichier __main.py__ et changer la variable __CONFIG_FILE__.
Par le chemin absolu vers le fichier de configuration. 
Cette partie sera prochainement améliorer dans une version ultérieure.
Et un repository Git va bientôt être crée pour facilité les versions.

<hr/>

## Utilisation du script
Une fois le tout configurer il suffit de lancer le script en python avec les paramètres qui vont bien.

Il existe 3 paramètres pour l'appelle du script :
- __input__ (-i ou --input), le chemin du fichier ou le dossier que l'on veut backup.
- __output__ (-o ou --output), le chemin du fichier en sortie (sans extension au fichier de préférence).
- __copy__  (-c ou --copy), Le chemin du __dossier__ vers le quel on veut faire une copy du fichier de backup.
    Ce paramètre n'est pas obligatoire.

<hr/>

## Mise en place du Crontab 
Pour la mise en place du crontab il faut seulement d'appeller le script avec les bons paramètres dans crontab:
```bash
crontab -e
```

Quelques exemples:
- https://fr.wikipedia.org/wiki/Cron (regarder la photo c'est le mieux pour comprendre)
- https://doc.ubuntu-fr.org/cron

Un petit exemple du crontab :
```bash
# Pour faire un backup tout les jours à 14h et 20h pour le dossier "/home/stifler/Dev/" et le mettre dans /media/msi-predator/HDD/Backup/ et le renomer stifler_Dev (en partie)
0 14,20 * * * python3 /home/stifler/Dev/Projet-perso/python_backup/backup_python/main.py -i /home/stifler/Dev/  -o /media/msi-predator/HDD/Backup/stifler-dev

```
